#!/usr/bin/env python
import random
import os
import time
import webbrowser
from datetime import datetime

# Determine le chemin de l'application
__location__ = os.path.realpath(os.path.join(os.getcwd(),os.path.dirname(__file__)))

# Declaration des variables
nb = 0
nv = 0
result = 0
z = 0

# Permet de mettre l'affichage de l'ecran a zero
def cls():
    os.system('cls' if os.name=='nt' else 'clear')

# Creation du fichier d'historique des parties avec entete
def create_historique():
	if os.path.exists(os.path.join(__location__, 'resultats.html')) == False:
		text_file = open(os.path.join(__location__, 'resultats.html'),"w")
		text_file.write("<html><title>Resultats jeu des multiplications</title>")
		text_file.write("<h2><p align=center><font color=#04B486>Resultats jeu des multiplications</font></p></h2>")
		text_file.write("<table border=1 align=center><tr><th>Date et heure</th>")
		text_file.write("<th>Nom du Joueur</th>")
		text_file.write("<th>Niveau de difficulte</th>")
		text_file.write("<th>Nombre de multiplications</th>")
		text_file.write("<th>Nombre de tentatives</th>")
		text_file.write("<th>Temps (en sec.)</th>")
		text_file.write("<th>Temps (en min.)</th>")
		text_file.write("<th>Note sur 20</th></tr>")
		text_file.close()

def start():
    today = time.asctime( time.localtime(time.time()) )
    chrono1 = datetime.now()
    cls()
    print('Ok ' + name + ', ce petit programme va t\'entrainer a faire des multiplications')
    print("Pour commencer dis moi combien veux-tu en faire ?")
    nb=input()

    print("Choisis ton niveau de difficulte (1: Facile / 2: Moyen / 3: Difficile) ?")
    nv=input()

    print("Ok pour " + str(nb) + " multiplications, c'est parti !")
    print("")
    print("===========================================================")
    print("")

    def difficile():
        global err,pt,tent
        err = 0
        for pt in range(nb):
            x = random.randint(11,100)
            y = random.randint(11,100)
            z = x*y
            print("Combien font " + x + " x " + y + " ?")
            result=input()

            while result != z:
                err = err+1
                print("Essaye encore: " + str(x) + " x " + str(y) + " ?")
                result=input()

            pt = pt+1
            print("EXACT! Le resultat est " + str(z))
            print("===>> Tu as " + str(pt) + " points <<===")
            print("")

    def moyen():
        global err,pt,tent
        err = 0
        for pt in range(nb):
            x = random.randint(2,10)
            y = random.randint(11,100)
            z = x*y
            print("Combien font " + x + " x " + y + " ?")
            result=input()

            while result != z:
                err = err+1
                print("Essaye encore: " + str(x) + " x " + str(y) + " ?")
                result=input()

            pt = pt+1
            print("EXACT! Le resultat est " + str(z))
            print("===>> Tu as " + str(pt) + " points <<===")
            print("")

    def facile():
        global err,pt,tent
        err = 0
        for pt in range(nb):
            x = random.randint(0,10)
            y = random.randint(0,10)
            z = x*y
            print("Combien font " + str(x) + " x " + str(y) + " ?")
            result=input()

            while result != z:
                err = err+1
                print("Essaye encore: " + str(x) + " x " + str(y) + " ?")
                result=input()

            pt = pt+1
            print("EXACT! Le resultat est " + str(z))
            print("===>> Tu as " + str(pt) + " points <<===")
            print("")

    if nv == 1:
        facile()

    if nv == 2:
        moyen()

    if nv == 3:
        difficile()

    tent = err+pt
    end(chrono1,today,name,nv,pt,tent)

def end(chrono1,today,name,nv,pt,tent):
    chrono2 = datetime.now()
    chrono = round((chrono2-chrono1).total_seconds(),1)
    chronom = round(chrono/60,2)
    note = round(float(pt)/float(tent)*100/5,1)
    create_historique()
    if nv == 1:
    	nv = "Facile"
    if nv == 2:
    	nv = "Moyen"
    if nv == 3:
    	nv = "Difficile"
    if result == z:
        text_file = open(os.path.join(__location__, 'resultats.html'),"a")
        text_file.write("\n<tr align=center><td>{}".format(today),)
        text_file.write("</td><td>{}".format(name))
        text_file.write("</td><td>{}".format(nv))
        text_file.write("</td><td>{}".format(pt))
        text_file.write("</td><td>{}".format(tent))
        text_file.write("</td><td>{}".format(chrono))
        text_file.write("</td><td>{}".format(chronom))
        text_file.write("</td><td>{}".format(note))
        text_file.write("</td><tr>")
        text_file.close()
        cls()
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("=======================================================================")
        print("============================= BRAVO !!!!! =============================")
        print("           >> Tu as reussi " + str(pt) + " multiplications en " + str(tent) + " tentatives << ")
        print("       >> pour un temps total de " + str(chrono) + " secondes soit " + str(chronom) + " minutes << ")
        print("                >> tu as obtenu la note de " + str(note) + " sur 20 << ")
        print("=======================================================================")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        print("")
        replay = raw_input("On rejoue (O/N) ?")
        if replay == "O" or replay == "o":
            start()
        else:
            cls()
            print("")
            print("")
            print("")
            print("")
            print("")
            print("")
            print("")
            print("")
            print("                maths.py - by @groussel - v1.7 - 21/2/19")
            print("                resultats detailles : " + os.path.join(__location__, 'resultats.html'))
            print("                Le programme va s'arreter dans 10s.")
            webbrowser.open(os.path.join(__location__, 'resultats.html'))
            time.sleep(10)

cls()
print("Hello !")
name = raw_input("Quel est ton prenom ? ")
start()