# Mathematiques

Programme Python permettant de réviser ses tables de multiplication.
Ce programme a d'abord été écrit en Python v2 avant d'être adapté en Python v3.

# Fonctionnalités

*  Permet de réviser les tables de mutiplications selon trois modes de difficulté :
    *  *Facile* : multiplications de type x(0-10) et y(0-10). Exemple : 3x7
    *  *Moyen* : multiplications de type x(2-10) et y(11-100). Exemple : 5x27
    *  *Difficile* : multiplications de type x(11-100) et y(11-100). Exemple : 32x69
*  Le joueur indique son pseudonyme et le nombre de multiplications souhaitées
*  Les points sont comptés ainsi que le nombre de tentatives
*  Les parties sont chronométrées
*  A la fin de la partie, une note sur 20 est adressée au joueur
*  L'historique des statistiques des parties est conservé dans un document resultat.html créé dans le répertoire du programme

# Evolutions possibles

*  Adapter le programme aux autres opérations mathématiques (additions, soustractions, divisions...)
*  *... ouvert aux propositions*